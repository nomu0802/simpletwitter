package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

    private int id;
    private int userId;
    private String text;
    private Date createdDate;
    private Date updatedDate;


    // getter/setterは省略されているので、自分で記述しましょう。

    public int getUserId() {
    	return id;

    }


    public void setUserId(int userId) {
    	this.userId=userId;
    }

    public String getText() {
    	return text;
    }

    public void setText(String text) {
    	this.text=text;
    }


}